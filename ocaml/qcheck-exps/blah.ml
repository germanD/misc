(* Hashes are integers for the moment *)
module type Hash = sig
  (** Pedersen's Commitment of a transaction output (i.e. address and value).
      A merkle tree is formed with the existing commitment and filled with
      a default uncommitted value **)
  type commitment

  type t = int64

  val uncommitted : height:int -> t

  (** Hash function to compute the merkle tree (Pedersen's hash on JubJub curve).
      Height is the height we are hashing at in the merkle tree. **)
  val merkle_hash : height:int -> t -> t -> t

  (** Hashes and commitments are the same object but are given different types
      to avoid confusing nodes and leaves. **)
  val of_commitment : commitment -> t

  val to_commitment : t -> commitment

end

let commit_gen_arg ~(max_height : int) =
  let open QCheck.Gen in
  (* Generate a list of random int64 for [uncommited] *)
  let us = generate ~n:max_height ui64 in
  (* Generate a list of random int64 for [merkle_hash] *)
  let hs = generate ~n:max_height ui64 in
  let genHash =
    (module struct
       type commitment = int64
       type t = int64
        let uncommitted ~(height:int) = List.nth us height
        let of_commitment x = x
        let to_commitment x = x
        let merkle_hash ~(height:int) _ _ = List.nth hs height
     end : Hash) in
  pure genHash
(* Do these two have the same semantics *)
  let commit_gen_arg2 ~(max_height : int) =
  let open QCheck.Gen in
  let genHash =
    (module struct
       type commitment = int64
       type t = int64
       (* Generate a list of random int64 for [merkle_hash] *)
       let hs = generate ~n:max_height ui64
       (* Generate a list of random int64 for [uncommited] *)
       let us = Format.eprintf "COUCOU@.";
                generate ~n:max_height ui64
       let uncommitted ~(height:int) = List.nth us height
       let of_commitment x = x
       let to_commitment x = x
       let merkle_hash ~(height:int) _ _ = List.nth hs height
     end : Hash) in
  pure genHash
  (* the latter is the one we don't want: generating a new uncommited
     list for each call to uncommited: *)
  let commit_gen_arg3 ~(max_height : int) =
  let open QCheck.Gen in
  let genHash =
    (module struct
       type commitment = int64
       type t = int64
       (* Generate a list of random int64 for [merkle_hash] *)
       let hs = generate ~n:max_height ui64
       let uncommitted ~(height:int) =
         (* Generate a list of random int64 for [uncommited] *)
       let us = Format.eprintf "COUCOU@.";
                generate ~n:max_height ui64 in
       List.nth us height
       let of_commitment x = x
       let to_commitment x = x
       let merkle_hash ~(height:int) _ _ = List.nth hs height
     end : Hash) in
  pure genHash


(* generate One instance of each module *)
let generate1_commitment_1 =
    QCheck.Gen.generate1 (commit_gen_arg ~max_height:32)

let generate1_commitment_2 =
  QCheck.Gen.generate1 (commit_gen_arg2 ~max_height:32)

let generate1_commitment_3 =
  QCheck.Gen.generate1 (commit_gen_arg3 ~max_height:32)

let uncommitted1 =
  let module M = (val generate1_commitment_1 : Hash)
  in M.uncommitted

let uncommitted2 =
  let module M = (val generate1_commitment_2 : Hash)
  in M.uncommitted

let uncommitted3 =
  let module M = (val generate1_commitment_3 : Hash)
  in M.uncommitted

(* Testing the Tests *)
(*
─( 15:41:39 )─< command 0 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # #require "qcheck";;
─( 15:41:39 )─< command 1 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # #use "blah.ml";;
module type Hash =
  sig
    type commitment
    type t = int64
    val uncommitted : height:int -> t
    val merkle_hash : height:int -> t -> t -> t
    val of_commitment : commitment -> t
    val to_commitment : t -> commitment
  end
val commit_gen_arg : max_height:int -> (module Hash) QCheck.Gen.t = <fun>
val commit_gen_arg2 : max_height:int -> (module Hash) QCheck.Gen.t = <fun>
val commit_gen_arg3 : max_height:int -> (module Hash) QCheck.Gen.t = <fun>
val generate1_commitment_1 : (module Hash) = <module>
COUCOU
val generate1_commitment_2 : (module Hash) = <module>
val generate1_commitment_3 : (module Hash) = <module>
val uncommitted1 : height:int -> int64 = <fun>
val uncommitted2 : height:int -> int64 = <fun>
val uncommitted3 : height:int -> int64 = <fun>
─( 15:42:01 )─< command 2 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # uncommitted1 10;;
- : int64 = -5098892802088410717L
─( 15:42:15 )─< command 3 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # uncommitted1 10;;
- : int64 = -5098892802088410717L
─( 15:42:24 )─< command 4 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # uncommitted2 10;;
- : int64 = -692067793108888526L
─( 15:42:25 )─< command 5 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # uncommitted2 10;;
- : int64 = -692067793108888526L
─( 15:42:29 )─< command 6 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # uncommitted3 10;;
COUCOU
- : int64 = -5855766751610681093L
─( 15:42:31 )─< command 7 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop # uncommitted3 10;;
COUCOU
- : int64 = -4398955920460193223L
─( 15:42:40 )─< command 8 >─────────────────────────────────────────────────────────────────────────────────────────{ counter: 0 }─
utop #
 *)
